import React,{Component} from 'react'
import 'antd/dist/antd.css';
import { Row, Col,Card } from 'antd';
import "./contactInfo.css"
import 'bootstrap/dist/css/bootstrap.min.css'
import Placeholder from "../img/placeholder.png"
import Phone from "../img/phone.png"
import Clock from "../img/clock.png"
import Mail from "../img/mail.png"
import '../css/responsive.css'

class ContactInfo extends Component{
    render() {
        return(
            <div style={{width:"99% ",paddingBottom:"20px"}}>
                <div className="w-100">
                    <div className="site-card-wrapper">
                        <Row gutter={5} className="site_row">
                            <Col className="col_first" span={10} xs={20} sm={20} md={20} lg={10} xl={10}>
                                <Card className="site_card"   bordered={true}>
                                    <h4>Contact info</h4>
                                    <ul>
                                        <div className="ul_div1 ">
                                            <li>
                                            <span>
                                                <img src={Placeholder} alt=""/>
                                            </span>
                                                <h5>Address</h5>
                                                <p>
                                                    25, Lomonosova St.<br/>
                                                    Moscow, Russia, 665087
                                                </p>
                                            </li>
                                            <hr/>
                                            <li>
                                            <span>
                                                <img src={Phone} alt=""/>
                                            </span>
                                                <h5>Phone</h5>
                                                <p>
                                                    +7 234 949-58-83<br/>
                                                    +7 239 585-58-61
                                                </p>
                                            </li>
                                            <hr/>
                                        </div>
                                        <div className="ul_div2  mt-5">
                                            <li>
                                            <span>
                                                <img src={Mail} alt=""/>
                                            </span>
                                                <h5>E-mail</h5>
                                                <p>
                                                    support@agorastore.com
                                                </p>
                                            </li>
                                            <hr/>
                                            <li>
                                            <span>
                                                <img src={Clock} alt=""/>
                                            </span>
                                                <h5>Working hours</h5>
                                                <p>
                                                    Monday-Friday: 8am-8pm.<br/>
                                                    Saturday 9am-1pm
                                                </p>
                                            </li>

                                        </div>



                                    </ul>
                                </Card>
                            </Col>

                            <Col className="col_second" span={10} xs={20} sm={20} md={20} lg={10} xl={10}>
                                <Card className="site_card"   bordered={true}>
                                    <h4>Contact info</h4>
                                    <div className="cw-form">
                                        <form action="#">
                                            <div className="input">
                                                <label htmlFor="fname">First Name</label>
                                                <input type="text" id="fname" name="fname"/>
                                            </div>
                                            <div className="input last-name">
                                                <label htmlFor="sname">Second Name</label>
                                                <input type="text" id="sname" name="sname"/>
                                            </div>
                                            <div className="text-area">
                                                <label htmlFor="text">Message</label>
                                                <textarea name="text" id="text"></textarea>
                                            </div>
                                            <div className="send-btn">
                                                <input type="submit" value="Send Message" name="send"/>
                                            </div>
                                        </form>
                                    </div>
                                </Card>
                            </Col>


                        </Row>
                    </div>

                </div>
            </div>
        )
    }

}
export default ContactInfo