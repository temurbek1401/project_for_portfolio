import React from 'react';
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
import 'antd/dist/antd.css';
import '../Css/first.css';
import { Layout } from 'antd';
import {Button, Col, Row} from "antd";
import {FaCircle,FaCalendarWeek,FaComment,FaAngleLeft,FaAngleRight} from 'react-icons/fa';
import {connect} from "react-redux";
import {enLanguage} from "../redux/Actions/enLanguage";
import {uzLanguage} from "../redux/Actions/uzLanguage";
const { Header, Footer, Content } = Layout;
class First extends React.Component{
    render() {
        const {uzLang}=this.props;
        return(
            <React.Fragment>
                <Layout>
                    <Content>
                        <div className="container1" name="yuqoriga">
                        <div className="header"><h4 style={{zIndex:'10000'}}>{uzLang?'TEZKOR XABARLAR:':'Breaking News:'}</h4>
                        {/*<span className="prev"><FaAngleLeft style={{marginTop:'3px'}}/></span>*/}
                        {/*<span className="next"><FaAngleRight style={{marginTop:'3px'}}/></span>*/}
                           <div>
                               <Slider className="sliderrrr" style={{width:'500px',overflow:'hidden'}} previousButton={<span className="prev"><FaAngleLeft /></span>} nextButton={<span className="next"><FaAngleRight /></span>}
                                       autoplay={true}
                               >
                                   <h6 style={{marginTop:'-5px',marginLeft:'550px'}}>Lorem ipsum dolor sit amet, con repellendus repudiandae </h6>
                                   <h6 style={{marginTop:'-5px',marginLeft:'550px'}}>Playoff flashbackNick Anderson strips MJ </h6>
                                   <h6 style={{marginTop:'-5px',marginLeft:'550px'}}>Lorem ipsum dolor sit amet, con repellendus repudiandae </h6>
                               </Slider>
                           </div>
                        </div>

                        {/*<p style={{position:'absolute',marginLeft:'250px',marginTop:'-23px',color:'black'}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>*/}
                        <Row>
                            <Col xs={24} sm={24} md={24} lg={12}><div className="first">
                                <img src="http://themeinnovation.com/demo2/html/newsupdate-preview/newsupdate/img/post-img/treading/trd-post-img1.jpg" alt="error"/>
                               <div className="first_info">
                                  <div className="first_little_info">
                                   <span className="first_icon">Sport <FaCircle/> </span>
                                   <span className="first-icon first_little_info" style={{color:'white'}}><FaCalendarWeek style={{color:'white'}}/>{uzLang?'12-Iyun':'June 12'}</span>
                                  <span className="first-icon first_little_info" style={{color:'white'}}><FaComment/> 15</span>
                                  </div>
                                   <a href="#" className="trd-post-title">Playoff flashbackNick Anderson strips MJ</a>
                               </div>
                            </div></Col>
                            <Col xs={24} sm={24} md={24} lg={12}>
                                <div className="second">
                                    <Row>
                                        <Col xs={24}><div className="second_1"><img src="http://themeinnovation.com/demo2/html/newsupdate-preview/newsupdate/img/post-img/treading/trd-post-img2.jpg" alt="error"/>
                                            <div className="first_info">
                                                <div className="first_little_info" style={{backgroundColor:'#00ABDC'}}>
                                                    <span className="first_icon">HI-Tech <FaCircle/> </span>
                                                    <span className="first-icon first_little_info" style={{color:'white',backgroundColor:'#00ABDC'}}><FaCalendarWeek style={{color:'white'}}/>{uzLang?'12-Iyun':'June 12'}</span>
                                                    <span className="first-icon first_little_info" style={{color:'white',backgroundColor:'#00ABDC'}}><FaComment/> 15</span>
                                                </div>
                                                <a href="#" className="trd-post-title">What People Like to Share More on Social</a>
                                            </div>
                                        </div></Col>

                                        <Col xs={24}>
                                            <div className="second_two">
                                                <Row>
                                                    <Col xs={24} sm={24} md={12} lg={12}><div className="second_2">
                                                        <img src="http://themeinnovation.com/demo2/html/newsupdate-preview/newsupdate/img/post-img/treading/trd-post-img3.jpg" alt="error"/>
                                                        <div className="first_info">
                                                            <div className="first_little_info" style={{backgroundColor:'#FD9F13'}}>
                                                                <span className="first_icon">Travel <FaCircle/> </span>
                                                                <span className="first-icon first_little_info" style={{color:'white',backgroundColor:'#FD9F13'}}><FaCalendarWeek style={{color:'white'}}/>{uzLang?'12-Iyun':'June 12'}</span>
                                                                <span className="first-icon first_little_info" style={{color:'white',backgroundColor:'#FD9F13'}}><FaComment/> 15</span>
                                                            </div>
                                                            <a href="#" className="trd-post-title">2015 Traveler Photo Contest</a>
                                                        </div>
                                                    </div></Col>
                                                    <Col xs={24} sm={24} md={12} lg={12}><div className="second_3">
                                                        <img src="http://themeinnovation.com/demo2/html/newsupdate-preview/newsupdate/img/post-img/treading/trd-post-img4.jpg" alt="error"/>
                                                        <div className="first_info">
                                                            <div className="first_little_info" style={{backgroundColor:'#FA4086'}}>
                                                                <span className="first_icon">Lifestyle <FaCircle/> </span>
                                                                <span className="first-icon first_little_info" style={{color:'white',backgroundColor:'#FA4086'}}><FaCalendarWeek style={{color:'white'}}/>June 12</span>
                                                                <span className="first-icon first_little_info" style={{color:'white',backgroundColor:'#FA4086'}}><FaComment/> 15</span>
                                                            </div>
                                                            <a href="#" className="trd-post-title">Kraft Liner Out of Crystals</a>
                                                        </div></div></Col>
                                                </Row>
                                            </div></Col>
                                    </Row>
                                </div>
                            </Col>
                        </Row>
                    </div></Content>
                </Layout>

            </React.Fragment>
        )
    }

}

const mapStateToProps = (state) => {

    return {
        uzLang: state.uzLang.uzLang,
    };
};
export default connect(mapStateToProps,{enLanguage,uzLanguage})(
    First
);