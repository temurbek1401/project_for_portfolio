import React,{Component} from 'react'
import 'antd/dist/antd.css';


import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

class Maps extends Component{
    render() {
        const style = {
            width:"100%",height:"440px",

        }

        return(
            <div style={{height:"450px"}}>
                <Map google={this.props.google} zoom={15}  style={style}
                     initialCenter={{
                         lat: 41.337445,
                         lng: 69.285328

                     }}  >

                    <Marker onClick={this.onMarkerClick}
                            name={'Current location'} />

                    <InfoWindow onClose={this.onInfoWindowClose}>

                    </InfoWindow>
                </Map>
            </div>
        )
    }
}


export default GoogleApiWrapper({
    apiKey: ("AIzaSyC9C2DqNhE0W--wYPmZjjJBQ3_gGHCatO4")
})(Maps)