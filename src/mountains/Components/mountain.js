import React,{Component} from 'react';
import img1 from './images/a.png';
import img2 from './images/b.jpg';
import '../Components/mounStyle.css';
import {Row,Col} from 'antd';
import {connect} from "react-redux";
import {enLanguage} from "../../redux/Actions/enLanguage";
import {uzLanguage} from "../../redux/Actions/uzLanguage";

class Mountain extends Component{
    render(){
        const {uzLang}=this.props;
        return(
            <div className='flex container' >
               <Row>
                   <Col md={12} sm={24} xs={24}>
                       <div  className='left'>
                           <img src={img2} style={{width:'350px'}}/>
                       </div>
                   </Col>
                   <Col md={12} sm={24} xs={24}>
                       <div className='right'>
                           <img src={img1} style={{width:'140px'}}/>
                           <p className='h5' style={{color:'#f26522'}}>{uzLang?"ILTIMOS KEYINROQ QAYTING":"PLEASE COME BACK LATER"}</p>
                           <h1>{uzLang?"Biz hozir tuzatish ishlarini olib boryapmiz":"We are currently under maintenance"}</h1>
                           <hr/><br/>
                           <p style={{color:'black'}}>Ut eget maximus leo. Aenean metus quam, tincidunt et tortor a, vulputate mollis eros. Nulla gravida ante quam, id condimentum odio fermentum vestibulum. Suspendisse vestibulum, nunc quis.Ut eget maximus leo. Aenean metus quam, tincidunt et tortor vulputate.</p>
                       </div>
                   </Col>
               </Row>


            </div>
        )
    }
}
const mapStateToProps = (state) => {

    return {
        uzLang: state.uzLang.uzLang,
    };
};
export default connect(mapStateToProps,{enLanguage,uzLanguage})(
    Mountain
)