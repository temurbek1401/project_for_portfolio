import React, {Component} from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import './css/error.css';
import {connect} from "react-redux";
import {enLanguage} from "../redux/Actions/enLanguage";
import {uzLanguage} from "../redux/Actions/uzLanguage";

class Error extends Component {
  render() {
    const {uzLang}=this.props;
    return (
      <div>
        <div className="error-area">
          <div className="error-bg"></div>
          <div className="error-content">
            <div className="container">
              <div className="row">
                <div className="col-md-12 col-sm-12 col-xs-12">
                  <div className="errors">
                    <h1>404</h1>
                    <h5>{uzLang?'BU SIZ IZLAYOTGAN WEB-SAHIFA EMAS':'THIS’S NOT THE WEB PAGE YOU’RE LOOKING FOR'}</h5>
                    <div className="please-try">
                      <p>{uzLang?"Iltimos quyidagi sahifalardan birini sinab ko'ring":"Please try one of the following pages"}</p>
                      <a className="please-try-submit blue-button-submit" href="homepage">{uzLang?'Bosh sahifaga qaytish':'Back To Homepage'}</a>
                    </div>
                    <div className="error-search">
                      <input className="search-input" type="text" placeholder={uzLang?'Izlash..':"Search.."} />
                        <input className="search-submit" type="submit" value={uzLang?"IZLASH":"SEARCH"} />

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="error-area-bottom">
            <div className="container">
              <div className="row">
                <div className="col-md-12 col-sm-12 col-xs-12">
                  <p>Copyright @2017<a target="_blank" href="#"> Newsupdate </a>All right reserved.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {

  return {
    uzLang: state.uzLang.uzLang,
  };
};
export default connect(mapStateToProps,{enLanguage,uzLanguage})(
    Error
)