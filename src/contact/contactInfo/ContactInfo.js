import React,{Component} from 'react'
import 'antd/dist/antd.css';
import { Row, Col,Card } from 'antd';
import "./contactInfo.css"
import 'bootstrap/dist/css/bootstrap.min.css'
import Placeholder from "../img/placeholder.png"
import Phone from "../img/phone.png"
import Clock from "../img/clock.png"
import Mail from "../img/mail.png"
import '../css/responsive.css'
import {connect} from "react-redux";
import {enLanguage} from "../../redux/Actions/enLanguage";
import {uzLanguage} from "../../redux/Actions/uzLanguage";

class ContactInfo extends Component{
    render() {
        const {uzLang}=this.props;
        return(
            <div style={{width:"99% ",paddingBottom:"20px"}}>
                <div className="w-100">
                    <div className="site-card-wrapper">
                        <Row gutter={5} className="site_row">
                            <Col className="col_first" span={10} xs={20} sm={20} md={20} lg={10} xl={10}>
                                <Card className="site_card"   bordered={true}>
                                    <h4>{uzLang?"Aloqa ma'lumotlari":"Contact info"}</h4>
                                    <ul>
                                        <div className="ul_div1 ">
                                            <li>
                                            <span>
                                                <img src={Placeholder} style={{width:'40px',marginLeft:'50px'}} alt=""/>
                                            </span>
                                                <h5>{uzLang?'Manzil':'Address'}</h5>
                                                <p>
                                                    {uzLang?"Amir Temur ko'chasi 106-uy":"25, Lomonosova St."}<br/>
                                                    {uzLang?'Toshkent,Uzbekistan 192927':'Moscow, Russia, 665087'}
                                                </p>
                                            </li>

                                            <li>
                                            <span>
                                                <img src={Phone} style={{width:'40px',marginLeft:'50px'}} alt=""/>
                                            </span>
                                                <h5>{uzLang?'Telefonlar':'Phone'}</h5>
                                                <p>
                                                    {uzLang?'+998990348672':'+7 234 949-58-83'}<br/>
                                                    {uzLang?'+998940040404':'+7 239 585-58-61'}
                                                </p>
                                            </li>

                                        </div>
                                        <div className="ul_div2  mt-5">
                                            <li>
                                            <span>
                                                <img src={Mail} style={{width:'40px',marginLeft:'50px'}} alt=""/>
                                            </span>
                                                <h5>E-mail</h5>
                                                <p>
                                                    {uzLang?'zakofrontendg21@gmail.com':' support@agorastore.com'}
                                                </p>
                                            </li>

                                            <li>
                                            <span>
                                                <img src={Clock} style={{width:'40px',marginLeft:'50px'}} alt=""/>
                                            </span>
                                                <h5>{uzLang?'Ish vaqti':'Working hours'}</h5>
                                                <p>
                                                    {uzLang?'Dushanba-Juma 8:00-18:00':' Monday-Friday: 8am-8pm.'}<br/>
                                                    {uzLang?'Shanba 9:00-13:00':'Saturday 9am-1pm'}
                                                </p>
                                            </li>

                                        </div>



                                    </ul>
                                </Card>
                            </Col>

                            <Col className="col_second" span={10} xs={20} sm={20} md={20} lg={10} xl={10}>
                                <Card className="site_card"   bordered={true}>
                                    <h4>{uzLang?"Aloqa ma'lumotlari":"Contact info"}</h4>
                                    <div className="cw-form">
                                        <form action="#">
                                            <div className="input">
                                                <label htmlFor="fname">{uzLang?'Ism':'First Name'}</label>
                                                <input type="text" id="fname" name="fname"/>
                                            </div>
                                            <div className="input last-name">
                                                <label htmlFor="sname">{uzLang?'Familiya':'Second Name'}</label>
                                                <input type="text" id="sname" name="sname"/>
                                            </div>
                                            <div className="text-area">
                                                <label htmlFor="text">{uzLang?'Xabar':'Message'}</label>
                                                <textarea name="text" id="text"></textarea>
                                            </div>
                                            <div className="send-btn">
                                                <input type="submit" value={uzLang?"Xabarni Yuborish":"Send Message"} name="send"/>
                                            </div>
                                        </form>
                                    </div>
                                </Card>
                            </Col>


                        </Row>
                    </div>

                </div>
            </div>
        )
    }

}
const mapStateToProps = (state) => {

    return {
        uzLang: state.uzLang.uzLang,
    };
};
export default connect(mapStateToProps,{enLanguage,uzLanguage})(
    ContactInfo
)