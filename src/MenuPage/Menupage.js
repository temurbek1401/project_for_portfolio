import React,{Component} from 'react';
import "./style.css";
import {
    SearchOutlined,
    MenuOutlined,
    PlusOutlined,
    DownOutlined,
    MinusOutlined,
    FieldTimeOutlined,
    FacebookOutlined,
    TwitterOutlined,
    GooglePlusOutlined, LinkedinOutlined, YoutubeOutlined
} from '@ant-design/icons';
import {Col,Row,Button,Input,Dropdown,Select} from "antd";
import {Sidebarmenu,Sports,Pages,Technology} from "./sidebarmenu"
import {Link} from "react-router-dom"
import {FaPinterest, FaVimeo, TiMediaEjectOutline} from "react-icons/all";
import { FaCloudMoonRain } from "react-icons/fa";
import {connect} from 'react-redux';
import {uzLanguage} from "../redux/Actions/uzLanguage";

import {enLanguage} from "../redux/Actions/enLanguage";
import {GetLanguage} from "../Utilitil";

const { Option } = Select;

class Menupage extends Component{
    state={
        minus:false,
    }
    render() {
        const {minus}=this.state;
        const {uzLang}=this.props;
        const salom=()=>{
            {
                document.querySelector(".menu1").classList.remove("remov")
            }
        };
        const xayr=()=>{
            {
                document.querySelector(".menu1").classList.add("remov")
            }
        };
        const sidebar=()=>{
            {
                document.querySelector(".pages").classList.add("remov")
                document.querySelector(".sidebarmenu").classList.toggle("remov")
            }
        };

        const pages=()=>{
            {
                document.querySelector(".pages").classList.toggle("remov")
            }
            const {minus}=this.state;
          this.setState({
              minus: minus? false:true,
          })
        }
        const search=()=>{
            {
                document.querySelector(".search").classList.toggle("remov")
            }
        }
        const sport=(
            <div style={{backgroundColor:'#252727',width:'150px',height:'150px',position:"absolute",left:'-28px'}}>
                {
                    Sports.map(item=>(
                        <tr>
                            <td className="sports"><a href={item.path}>{uzLang?item.nameUz:item.nameEn}</a></td>
                        </tr>
                    ))
                }
            </div>
        )
        const texnology=(
            <div style={{backgroundColor:'#252727',width:'220px',height:'180px',position:"absolute",left:'-28px'}}>
                {
                    Technology.map(item=>(
                        <tr>
                            <td className="sports"><a href={item.path}>{uzLang?item.nameUz:item.nameEn}</a></td>
                        </tr>
                    ))
                }
            </div>
        )

        const changeSelect=(value)=>{
            if (value){
                this.props.uzLanguage();
            }else {
                this.props.enLanguage();
            }
        }
        return(
            <React.Fragment>
         <div className="weater-top" name="yuqoriga" >
             <div className="container" >
                 <Row>
                     <Col md={12} sm={24} xs={24}>
                         <div className="weather-area" style={{color:'#8D8D8D'}}>
                             <span style={{fontSize:'20px',top:'2px'}}><FieldTimeOutlined /></span>
                             <span style={{left:'40px'}}>{uzLang?'19.02.2021 JUMA':'Friday,February 19,2021'}</span>
                             <span style={{left:'220px',fontSize:'20px',top:'0'}}><FaCloudMoonRain/></span>
                             <span style={{left:'250px'}}>28 <sup>o</sup>C</span>
                         </div>
                     </Col>
                     <Col md={12} sm={24} xs={24}>
                         <div className="ijtiomoiy-tarmoqlar">
                             <a href="#"><FacebookOutlined style={{color:'#5F6565',marginRight:'50px',marginLeft:'30px'}}/></a>
                             <a href="#"><TwitterOutlined style={{color:'#5F6565',marginRight:'50px'}}/></a>
                             <a href="#"><GooglePlusOutlined style={{color:'#5F6565',marginRight:'50px'}}/></a>
                             <a href="#"><FaVimeo style={{color:'#5F6565',marginRight:'50px',marginTop:'5px'}}/></a>
                             <a href="#"><YoutubeOutlined style={{color:'#5F6565',marginRight:'50px',marginTop:'5px'}}/></a>
                             <a href="#"><Select onChange={changeSelect} defaultValue={!GetLanguage()?"UZ":"EN"}  style={{border:'2px solid black',width:'80px',color:'black',position:'absolute',right:'20px',fontSize:'15px',cursor:'pointer'}}>
                                 <Option value={false}  ><p style={{color:'black'}} >EN</p></Option>
                                 <Option value={true}  ><p style={{color:'black'}} >UZ</p></Option>

                             </Select></a>
                         </div>
                     </Col>
                 </Row>

                 <hr  style={{height:'0.01px',width:'100%',color:'#5F6565',backgroundColor:'#E8E9E9'}}/>
             </div>
         </div>
         <div className="container" style={{width:'100%'}}>
             <Row>
                 <Col md={8} sm={24} xs={24}>
                     <img style={{width:'290px',margin:'60px auto'}} src="http://themeinnovation.com/demo2/html/newsupdate-preview/newsupdate/img/logo/logo.png" alt="reklama1"/>
                 </Col>
                 <Col md={16} sm={24} xs={24}>
                     <img style={{marginTop:'30px',width:'100%'}} src="http://themeinnovation.com/demo2/html/newsupdate-preview/newsupdate/img/banner/header-top-banner1.jpg" alt="reklama1"/>
                 </Col>
             </Row>
         </div>
                <div className="fotermenu"  >
                   <div  className="foter" >
                       <p className="icons menuicon close" style={{color:'white',fontSize:'30px'}}>MENU</p>
                       <p onClick={sidebar} className="icons close" style={{color:'white',fontSize:'30px'}}><MenuOutlined/></p>
                       <div className="container">
                           <div className="menu" >
                               <ul>
                                   <li><a href=""><Link to="/home/homepage">{uzLang?'BOSH SAHIFA':'HOMEPAGE'}</Link></a></li>
                                   <li onMouseMove={xayr} ><a href=""><Link to="/home/fashion">{uzLang?'DIZAYN':'FASHION'}</Link></a></li>
                                   <li><Dropdown   trigger={['click']}  overlay={texnology}>
                                       <a className="ant-dropdown-link" >
                                           {uzLang?'TEXNOLOGIYA':'  TECH'} <DownOutlined />
                                       </a>
                                   </Dropdown></li>
                                   <li><Dropdown   trigger={['click']}  overlay={sport}>
                                       <a className="ant-dropdown-link" >
                                           {uzLang?'SPORT':' Sports'} <DownOutlined />
                                       </a>
                                   </Dropdown></li>
                                   <li onMouseMove={xayr}><a href=""><Link to="/home/travel">{uzLang?'SAYOHAT':'TRAVEL'}</Link></a></li>
                                   <li onMouseMove={xayr}><a href=""><Link to="/home/aboutus">{uzLang?'BIZ HAQIMIZDA':'ABOUT US'}</Link></a></li>
                                   <li onMouseMove={xayr}><a href=""><Link to="/home/contact">{uzLang?"BOG'LANISH":"CONTACT"}</Link></a></li>
                                   <li onMouseMove={salom}><a href=""><Link to="/home/page">{uzLang?'SAHIFALAR':'PAGE'}</Link></a></li>
                                   <li><p onClick={search} style={{color:'white',fontSize:'20px',fontFamily:'Montserrat',cursor:'pointer',marginLeft:'40px'}}  ><SearchOutlined/></p></li>
                               </ul>
                           </div>
                       </div>
                   </div>
                   <div onMouseLeave={xayr} className="menu1 remov">
                       <Row style={{marginTop:'20px',marginLeft:'20px'}}>
                           {
                               Pages.map(bb=>(
                                   <Col md={8}>
                                       <a style={{cursor:'pointer'}} href="#"><Link to={bb.path}>{uzLang?bb.nameUz:bb.nameEn}</Link></a>
                                   </Col>
                               ))
                           }
                       </Row>
                   </div>
                   <input className="search remov " type="search" placeholder={uzLang?'Bu yerdan izlang':'"Search here"'}/>
               </div>

                <div className="sidebarmenu remov">
   <Row>
           <Col className="item" sm={24} >
               <a href="/homepage">{uzLang?'BOSH SAHIFA':'HOMEPAGE'}</a>
           </Col>
       <Col className="item" sm={24}>
           <a href="/fashion">{uzLang?'DIZAYN':'FASHION'}</a>
       </Col>
       <Col className="item" sm={24}>
           <Dropdown   trigger={['click']}  overlay={texnology}>
               <a className="ant-dropdown-link" >
                   {uzLang?'TEXNOLOGIYA':'  TECH'} <DownOutlined />
               </a>
           </Dropdown>
       </Col>
       <Col className="item" sm={24}>
               <Dropdown   trigger={['click']}  overlay={sport}>
                   <a className="ant-dropdown-link" >
                       {uzLang?'SPORT':' Sports'} <DownOutlined />
                   </a>
               </Dropdown>
           </Col>
       {
           Sidebarmenu.map(e=>(
                   <Col className="item" sm={24}>
                       <a href={e.path}>{uzLang?e.nameUz:e.nameEn}</a>
                   </Col>
           ))
       }
            <Col className="item" sm={24}>
                <a style={{float:'left'}} href="">PAGES</a>
                <a onClick={pages} style={{position:'absolute', right:'20px', fontSize:'18px'}}>{!minus?<PlusOutlined />:<MinusOutlined/>}</a>
            </Col>
     </Row>
   </div>
                <div className="pages remov">
                    <Row>
                        {
                            Pages.map(item=>(
                                <div className="itemm">
                                    <Col sm={24}>
                                        <a href={item.path}>{uzLang?item.nameUz:item.nameEn}</a>
                                    </Col>
                                </div>
                            ))
                        }
                    </Row>
                </div>
            </React.Fragment>
        )
    }
}
const mapStateToProps = (state) => {

    return {
        uzLang: state.uzLang.uzLang,
    };
};
export default connect(mapStateToProps,{enLanguage,uzLanguage})(
    Menupage
)