export let Sidebarmenu=[
    {
        path:'/travel',
        nameEn:'TRAVEL',
        nameUz:'SAYOHAT',
    },
    {
        path:'/home/contact',
        nameEn:'CONTACT',
        nameUz:'ALOQA',
    },
    {
        path:'/aboutus',
        nameEn:'ABOUT US',
        nameUz:'BIZ HAQIMIZDA',
    },
]
export  let Sports=[
    {
        path:'/futbol',
        nameUz:"Futbol",
        nameEn:"FOOTBALL"
    },
    {
        path:'/voleybol',
        nameUz:"VOLEYBOL",
        nameEn:"VOLLEYBALL"
    },
    {
        path:'/boks',
        nameUz:"BOKS",
        nameEn:"BOXING",
    },
]
export let Technology=[
    {
        path:'/robototexnika',
        nameUz:'ROBOTOTEXNIKA',
        nameEn:'ROBOTOTECH'
    },
    {
        path:'/webdasturlash',
        nameUz:'WEB DASTURLASH',
        nameEn:'WEB DEVELOPMENT'
    },
    {
        path:'/avtomobillar',
        nameUz:'AVTOMOBILLAR',
        nameEn:'CARS'
    }
]
export let Pages=[
        {
            path:'/home/homepage',
            nameEn:'HOME ONE',
            nameUz:'BOSH SAHIFA'
        },
        {
            path:'/home/blogcolumn',
            nameEn:'BLOG STANDART',
            nameUz:'STANDART QISM'
        },
        {
            path:'/home/blogdetails',
            nameEn:'BLOG DETAILS',
            nameUz:'BLOG TAFSILOTLARI'
        },
        {
            path:'/coming_soon',
            nameEn:'COMING SOON',
            nameUz:'TEZ KUNDA'
        },
        {
            path:'/home/blogcolumn',
            nameEn:'BLOG COLUMN TWO',
            nameUz:'BLOG USTUNI'
        },
        {
            path:'/home/aboutus',
            nameEn:'About US',
            nameUz:'BIZ HAQIMIZDA'
        },
        {
            path:'/home/maintenance',
            nameEn:'MAINTENANCE',
            nameUz:"TEXNIK XIZMAT KO'RSATISH"
        },
        {
            path:'/home/blogdetails/withsidebar',
            nameEn:'BLOG DETAILS WITH SIDEBAR',
            nameUz:'BLOG TAFSILOTLARI'
        },
        {
            path:'/home/contact',
            nameEn:'CONTACT',
            nameUz:'ALOQA'
        },
        {
            path:'/home/error',
            nameEn:'404',
            nameUz:'404'
        },
]
